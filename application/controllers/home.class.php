<?php

class Home extends Controller 
{
	
public function __construct($controller, $action, $id) {
	
  parent::__construct($controller, $action, $id);
  $method_name = "$this->action";
  $this->$method_name();
}
		
public function index() {
  $this->set('title', 'IPframe > Home');
  $this->view();
}
	
}

/* End of file home.class.php */
/* Location: ./application/controllers/home.class.php */