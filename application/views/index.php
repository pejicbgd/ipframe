<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
	<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" >
<meta http-equiv="Content-Type" content="text/html" charset="Windows-1250">
<title><?php echo $title; ?></title>

<meta name="Author" content="Ivan Pejic" lang="sr" xml:lang="sr" />
<meta name="Copyright" content="Ivan Pejic" lang="sr" xml:lang="sr" />
<meta name="Description" content="Custom PHP framework, light footprint, MVC architechture, SEO frendly urls" lang="sr" xml:lang="sr" />
<meta name="Keywords" content="Custom PHP framework, ivan pejic, light footprint, MVC architechture, SEO frendly urls" lang="sr" xml:lang="sr" />
<meta property="og:title" content="Custom PHP framework"/>
<meta property="og:type" content="article"/>
<meta property="og:url" content="http://www.github.com/pejicbgd/ipframe"/>
<meta property="og:site_name" content="IPframe"/>
<meta property="og:description" content="Custom PHP framework, light footprint, MVC architechture, SEO frendly urls"/>
<link href="<?php echo $base_url; ?>/css/index.css" type="text/css" rel="stylesheet" media="screen" />
<link href="<?php echo $base_url; ?>/css/reset.css" type="text/css" rel="stylesheet" media="screen" />

<!--[if IE 6]>
<link href="http://www.blic.rs/resources/styles/iecrap.css?v=3" type="text/css" rel="stylesheet" media="screen" />
<![endif]-->

<script type="text/javascript" src="<?php echo $base_url;  ?>/public/css/jquery-1.11.1.js"></script>

</head>
<body>
  <div id="main">
    <div id="container">
      <h2>Welcome to this empty pages</h2>
      <p>This is the default controller for this application.</p>
      <p>You can find this page in:</p>
      <h3>./application/views/index.php</h3>
      <p>Controller for this page can be found in:</p>
      <h3>./application/controllers/home.class.php</h3>
      <p>Read about some basic usage guidelines in:</p>
      <h3>./readme.md</h3>
    </div>
  </div>
</body>
</html>