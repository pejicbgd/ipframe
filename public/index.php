<?php

//define some constants we will be using throughout
//avoid backslash forwardslash confusion
define('DS', DIRECTORY_SEPARATOR);

//define path to root folder
define('ROOT', dirname(dirname(__FILE__)));

//define path to app folders
define('PUBLIC_PATH', ROOT .DS .'public' .DS);
define('LIBRARY_PATH', ROOT .DS .'library' .DS);
define('APPLICATION_PATH', ROOT .DS .'application' .DS);

//and away we go...
require LIBRARY_PATH .'bootstrap.php';

