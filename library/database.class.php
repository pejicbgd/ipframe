<?php
/**
* class that wraps database functionality
*
*/
class Database
{

//variables that hold db parameters
private $username,
        $password,
        $host,
        $db_name;

//variables for database state
public $mysqli,
       $stmt,
       $affected_rows,
       $num_rows;

/**
* we use constructor to assign values to
* some class variables
* @return void
*/
public function __construct() 
{
  //load database configuration file
  include ROOT .DS .'config' .DS .'database.php';

  $this->username = $db['username'];
  $this->password = $db['password'];
  $this->host     = $db['host'];
  $this->db_name  = $db['db_name'];
  $this->connect();
}

/**
* function that return database connection object
*
* @return object $this->mysqli (connection object)
*/
private function connect()
{
  //establish new connection
  $this->mysqli = new mysqli($this->host, $this->username, $this->password, $this->db_name);
  
   //if there was an error connecting, abort execution
  if ($this->mysqli->connect_error) {
    trigger_error('Failed to connect to MySQL, with error: ' .$this->mysqli->connect_error, E_USER_NOTICE);
  } else {
      //return connection object
      return $this->mysqli;
    }
}

/**
* preparing sql statements for use
* 
* @param string $sql (sql statement)
* @return object $this->stmt
*/
public function prepare($sql)
{
  //reset values for affected_rows and num_rows
  $this->affected_rows = -1;
  $this->num_rows = 0;

  //check if connection has been established
  if (is_object($this->mysqli))
  {
    $this->stmt = $this->mysqli->prepare($sql);
    return $this;
  } else {
      throw new Exception("Is connection established?");
    }
} 

/**
* execute prepared statements
* 
* @param string $sql (sql statement)
* @return object $this
*/
public function execute() 
{
  //check if connection has been established and sql prepared
  if (is_object($this->mysqli) && is_object($this->stmt)) {
  
  //if we have some arguments coming with call
  if (count($args = func_get_args()) > 0) {
    
    $types  = array();
    $params = array();

    foreach ($args as $arg) {
      $types[]  = is_int($arg) ? 'i' : (is_float($arg) ? 'd' : 's');
      $params[] = $this->mysqli->real_escape_string($arg);
    }

    //put variable types in front of their names
    array_unshift($params, implode($types));

    //bind prepared statement
    $this->stmt->bind_param($params);
  }
     
  if ($this->stmt->execute()) {
          $this->affected_rows = $this->stmt->affected_rows;
          return $this;
        } else {
            throw new Exception("Error executing statement");
          }
    } else {
        throw new Exception("Is connection established?");
      }
}

/**
* fetch results for given statement
* 
* @param string $method 
* @return array $results
*/
public function result($method = 'assoc')
{
  //check if connection has been established and sql prepared
  if (is_object($this->mysqli) && is_object($this->stmt))
  
  //determine is statement result from database, or prepared stmt
  $stmt_type = get_class($this->stmt);
  switch ($stmt_type) {
  
    case 'mysqli_stmt':
    $result = $this->stmt->get_result();
    $close  = 'close()';
    break;

    case 'mysqli_result':
    $result = $this->stmt;
    $close  = 'free()';
    break;
    
    default:
    throw new Exception();
  }

  $this->num_rows = $result->num_rows;

  switch ($method) {
 
    case 'assoc':
    $method = 'fetch_assoc';
    break;

    case 'row':
    $method = 'fetch_row';
    break;

    default:
    $method = 'fetch_array';
    break;
  }
  
  //NEW array to hold results 
  $results = array();
  
  while ($row=$result->$method())
    $results[] = $row;

  $result->$close();
  return $results;
}

/**
* get number of affected rows for 
* previous executed sql statement
* 
* @return int $_affected_rows
*/
public function affected_rows()
{
  return $this->affected_rows;
}

/**
* get number of rows that result holds
* 
* @return int $_num_rows
*/
public function num_rows()
{
  return $this->num_rows;
}

}

/* End of file database.class.php */
/* Location: ./library/database.class.php */
