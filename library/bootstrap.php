<?php

//ensure session is active
if (session_id() == '') {
  session_start();
}

include ROOT .DS .'config' .DS .'config.php';

require LIBRARY_PATH .'shared.class.php';

//autoloading neccessary classes
function __autoload($class) 
{

	if (file_exists(LIBRARY_PATH .strtolower($class) .'.class.php'))
	{
		include LIBRARY_PATH .strtolower($class) .'.class.php';
	}
	if (file_exists(APPLICATION_PATH .'controllers' .DS .strtolower($class) .'.class.php'))
	{
		include APPLICATION_PATH .'controllers' .DS .strtolower($class) .'.class.php';
	}
	if (file_exists(APPLICATION_PATH .'models' .DS .strtolower($class) .'.class.php'))
	{
		include APPLICATION_PATH .'models' .DS .strtolower($class) .'.class.php';
	}
}

//call some important functions
Shared::setErrorReporting();

Shared::removeMagicQuotes();

Shared::unregisterGlobals();

Shared::callHook();

//end session to speed up concurrent connections
session_write_close();

/* End of file bootstrap.php */
/* Location: ./library/bootstrap.php */