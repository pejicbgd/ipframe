<?php

class Controller 
{

/**
*
* Parent controller for application
* its important to maintain flow of
* controller/action pair through this class
*
*/
public  $controller,
        $action,
        $id,
        $template,
        $db;	
/**
*
* assign controller/action pair to private variables
*
* @param $_controller string (controller being called)
* @param $_action string (action being called)
* @return void
*/
public function __construct($controller, $action, $id) 
{
  global $config;

  $this->controller = $controller;
  $this->action     = $action;
  $this->id         = $id;
  $this->template   = new Template($this->controller, $this->action);
  
  //connect to database is autoconnect is true
  if ($config['db_autoconnect'] == true) { 
    $this->db = new Database();
  }
}

/**
*
* function to set custom variables to be
* used in views. calls template class function 
* which places them in a _data array
*
* @param $name (name of variable)
* @param $value (value of variable)
* @return void
*/
public function set($name, $value)
{
  $this->template->set($name, $value);
}

/**
*
* function to load required views
*
* @param $path string (path where view file resides)
* @return void
*/
public function view($path='') 
{
  $this->template->render($path);
}

}

/* End of file controller.class.php */
/* Location: ./library/controller.class.php */