<?php

class Device
{
  
private $user_agent,
        $device,
        $browser,
        $device_length,
        $browser_length;

public function __construct() 
{
  $this->user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
  $this->device = array('iphone', 'ipad', 'android', 'silk', 'blackberry', 'touch');
  $this->browser = array('firefox', 'chrome', 'opera', 'msie', 'blackberry', 'trident');
  $this->device_length = count($this->device);
  $this->browser_length = count($this->browser); 
}

public function findDevice()
{
  for ($i = 0 ; $i < $this->device_length; $i++) {
    if (strstr($this->user_agent, $this->device[$i])) {
      return $this->device[$i];
    }
  }
}

public function findBrowser()
{
  for ($i = 0 ; $i < $this->browser_length; $i++) {
    if (strstr($this->user_agent, $this->browser[$i])) {
      return $this->browser[$i];
    }
  }
}

}