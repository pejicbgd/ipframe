<?php

class Template 
{

/**
*
* Parent template class for views
* main function is render, which loads
* called view, and passes set variables
*
*/
public  $data= array();
private $controller,
        $action;

public function __construct($controller, $action)
{
  $this->controller = $controller;
  $this->action     = $action;
}

/**
*
* function for setting custom variables
*
* @param $name sring (name of the variable)
* @param $value (value of the variable)
* @return void
*
*/
public function set($name, $value)
{
  $this->data[$name] = $value;
}

/**
*
* function that loads called view file
*
* @param $path string (path to view file)
* @return void
*
*/
public function render($path='')
{
  //import global $config array for extraction
  global $config;
  //if there is a collision,dont overwrite existing variable
  extract($this->data, EXTR_SKIP);
  extract($config, EXTR_SKIP);
  
  //if home controller is calling view
  if ($path == '') {
    include APPLICATION_PATH .'views' .DS .'index.php';
  //else load from subfolder where files reside
  } else {
      include APPLICATION_PATH .'views' .DS .$path;
    }
}

}

/* End of file template.class.php */
/* Location: ./library/template.class.php */