<?php

class Shared 

{

/**
*
* Class that handles general functionality
* and decides how url should be passed
*
*/
public static $controller,
              $action,
              $id;

/**
*
* Set error reporting depending on enviroment
* in development enviroment, all error should be visible
* in production env. we hide errors from end users, but we log them
*
* @return void
*/
public static function setErrorReporting() 
{

  if (DEVELOPMENT_ENVIROMENT == true) {
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');

  } else {
      error_reporting(E_ALL);
      ini_set('display_errors', 'Off');
      ini_set('log_errors', 'On');
      //path to log file
      ini_set('log_path', ROOT .DS .'tmp' .DS .'error.log');
    }
}

/**
*
* stripping slashes from input value
*
* @param $value string 
* @return $value string
*/
private function stripSlashes($value) 

{
  $value = is_array($value) ? array_map('strip_slashes', $value) : stripslashes($value);
  return $value;
}
 
/**
*
* remove magic quotes from GET, POST and COOKIE arrays
* 
* @return void
*/
public static function removeMagicQuotes() 
{
  if (get_magic_quotes_gpc()) {
    $_GET    = self::stripSlashes($_GET   );
    $_POST   = self::stripSlashes($_POST  );
    $_COOKIE = self::stripSlashes($_COOKIE);
  }
}
 
/**
*
* destroy variables from global arrays
* if 'register_globals' parameter is ON
* 
* @return void
*/
public static function unregisterGlobals() 
{
  //check register_globals from php.ini file
  if (ini_get('register_globals')) {
    $array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_ENV', '_FILES');
    foreach ($array as $value) {
      foreach ($GLOBALS[$value] as $key => $var) {
        if ($var === $GLOBALS[$key]) {
          unset($GLOBALS[$value][$key]);
		}
      }
    }
  }
}

/**
*
* hart of the operation - function that handles called url,
* and load appropriate classes based on that request
* 
* @return void
*/
public static function callHook() 
{
  global $config;
  
  $url = $_SERVER['REQUEST_URI'];
  $segments = explode('/', $url);

  //if this is home page url
  if ($url == '/') {
    self::$controller = $config['default_controller']; 
    self::$action = 'index';
  }

  //case when we have just controller called
  else if (count($segments) ==2) {
    self::$controller = strtolower($segments[1]);
	  self::$action = 'index';
  }

  //case when we have controller/action 
  else if (count($segments) == 3) {
    self::$controller = strtolower($segments[1]);
    self::$action     = strtolower($segments[2]);
  }
	
  //case when we have controller/action/id
  else if (count($segments) == 4) {
    self::$controller = strtolower($segments[1]);
    self::$action     = strtolower($segments[2]);
    self::$id         = strtolower($segments[3]);
  }

  $path = APPLICATION_PATH .'controllers' .DS .self::$controller .'.class.php';

  $methods = get_class_methods(self::$controller);

  //check if there are controller by that name, 
  //and if method exist within that controller
  if (file_exists($path) && in_array(self::$action, $methods)) {	
	
    //choosing class name depending on controller called
    include_once $path;
		
    //instancing controller called
    $caller = new self::$controller(self::$controller, self::$action, self::$id);
		
  } else {

      include APPLICATION_PATH .'404.php';
		
	}

}

}

/* End of file shared.class.php */
/* Location: ./library/shared.class.php */