<?php

$db = array();

//username to connect to database
$db['username'] = '';

//password to connect to database
$db['password'] = '';

//host where database is located
$db['host'] = '';

//name of the database
$db['db_name'] = '';

//path to folder where database backup
//will be stored
$db['backup_path'] = ROOT .DS .'tmp' .DS;

/* End of file database.php */
/* Location: ./config/database.php */