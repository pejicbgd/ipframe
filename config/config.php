<?php

//set enviroment for error reporting
define('DEVELOPMENT_ENVIROMENT', true);

$config = array();

//variable that holds base address with backslash
$config['base_url'] = 'http://localhost';

//default controller for application
$config['default_controller'] = 'home';

//connect to database on app load
$config['db_autoconnect'] = true;

//time in seconds for cookie to last 7200 sec= 2 hours
$config['cookie_expiration'] = 7200;

//does cookie expire on close
$config['cookie_expire_close'] = true;

//domain in which cookie is valid
$config['cookie_domain'] = 'localhost';

//cookie path, common is /
$config['cookie_path'] = '/';

/* End of file config.php */
/* Location: ./config/config.php */